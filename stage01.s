ToHex:                              ; in ax, out [es:di]
    Def .data, .digits, db '0123456789ABCDEF'
    pusha
    mov bx, .digits
    mov dx, ax                      ; value
    mov cx, 4
.loop:
    rol dx, 4
    mov al, dl
    and al, 0x0F
    xlatb
    stosb
    loop .loop
    mov al, 'h'
    stosb
    popa
    ret

Sercon.base: equ 0x3F8

SerconPrint:                        ; in cx [ds:si]
    pusha
.loop:
    lodsb
    call SerconWrite
    loop .loop
    popa
    ret

SerconWrite:                        ; in al
    push dx
    push ax
.wait:
    In al, Sercon.base+5
    test al, 0x20
    jz .wait
.send:
    pop ax
    mov dx, Sercon.base
    out dx, al
    pop dx
    ret
