.POSIX:

run: boop
	qemu-system-i386 -nographic boop

dump: boop
	> dump.txt ndisasm boot.o -o 0x7C00
	>> dump.txt ndisasm boop.o -o 0x10000

boop: boot.o boop.o
	> $@ cat boot.o boop.o

boop.o boop.map: boop.s macros/debug.s macros/portio.s macros/sections.s macros/segments.s
	nasm -f bin -o boop.o boop.s

boot.o boot.map: boot.s macros/debug.s macros/portio.s macros/sections.s macros/segments.s
	nasm -f bin -o boot.o boot.s

clean:
	rm -f boop *.o *.map

.PHONY: run text data clean
