[map boop.map]
bits 16
org 0x10000

section .data follows=.text
section .end start=0x20000
    nop
section .text

%include "macros/debug.s"
%include "macros/portio.s"
%include "macros/sections.s"
%include "macros/segments.s"

%macro DebugDumpRegs 1
%%eip: equ -DebugPushRegs.len+DebugPushRegs.eip
%%esp: equ -DebugPushRegs.len+DebugPushRegs.esp
%%ebp: equ -DebugPushRegs.len+DebugPushRegs.ebp
%%start:
    enter 0, 0
%%ip:
    DebugPushRegs
    sub word [bp+%%eip], %%ip-%%start   ; adjust IP for ENTER
    add word [bp+%%esp], 2          ; adjust SP for push by ENTER
    mov ax, [bp]                    ; grab correct BP from ENTER
    mov [bp+%%ebp], ax
    pusha
    BeginWithSegment fs, ss
        lea si, [bp-DebugPushRegs.len]
        mov cx, %1
        call DumpRegs
    EndWithSegment
    popa
    leave
%endmacro

Stage1:
    SerconPrintLiteral `>>> stage1\n`
    SerconPrintLiteral `InitRegs: registers on boot:\n`
    pusha
    BeginWithSegment fs, 0x0000
        mov si, 0x1000
        mov cx, 4
        call DumpRegs
    EndWithSegment
    popa
    call BiosDetectMemory
    call TweakMemoryMap
.pic:
    call Pic
    SerconPrintLiteral `Stage1.pic: installing ISR for IRQ0: PitTick\n`
    BeginWithSegment fs, 0x0000
        mov di, 0xF0*4              ; IRQ0 @ INT F0h
        mov word fs:[di+0], PitTick
        mov word fs:[di+2], 0x1000
    EndWithSegment
.idle:
    SerconPrintLiteral `Stage1.idle: reached end of function\n`
    jmp Idle

ToHex64:                            ; in edx:eax, out [es:di]
    pusha
    xchg eax, edx
    call ToHex32
    add di, 8
    xchg eax, edx
    call ToHex32
    popa
    ret

ToHex32:                            ; in eax, out [es:di]
    o32 pusha
    mov bx, ToHex.digits
    mov edx, eax                    ; value
    mov cx, 8
.loop:
    rol edx, 4
    mov al, dl
    and al, 0x0F
    xlatb
    stosb
    loop .loop
    mov al, 'h'
    stosb
    o32 popa
    ret

TweakMemoryMap:
    enter 34, 0
    o32 pusha
    SerconPrintLiteral `TweakMemoryMap:\n`
    mov si, BiosDetectMemory.map
    mov cx, BiosDetectMemory.capacity
.loop:
    push cx
        lea di, [bp-18]
        mov cx, 17
        lodsd
        mov [bp-34], eax
        lodsd
        mov [bp-30], eax
        lodsd
        mov [bp-26], eax
        lodsd
        mov [bp-22], eax
        or eax, [bp-26]
        jz .skip                    ; skip if length = 0
        cmp dword [bp-30], 0
        ja .ok                      ; no tweak if base >= 100000000h
        cmp dword [bp-34], 0x30000
        ja .ok                      ; no tweak if base >= 30000h
        mov eax, 0x30000
        sub eax, [bp-34]
        add [bp-34], eax            ; clamp base to at least 30000h
        sub [bp-26], eax            ; adjust length by same amount
        sbb dword [bp-22], 0
.ok:
        SerconPrintLiteral `    base `
        push si
            mov si, di
            mov eax, [bp-34]
            mov edx, [bp-30]
            CallWithEs ss, ToHex64
            CallWithDs ss, SerconPrint
        pop si
        SerconPrintLiteral ` length `
        push si
            mov si, di
            mov eax, [bp-26]
            mov edx, [bp-22]
            CallWithEs ss, ToHex64
            CallWithDs ss, SerconPrint
        pop si
        SerconPrintLiteral `\n`
.skip:
    pop cx
    dec cx
    jnz .loop
    o32 popa
    leave
    ret

BiosDetectMemory:
.capacity: equ 16
.smap: equ 0x534D4150
    Def .data, .map, times .capacity*16 db 0
    enter 46, 0
    o32 pusha
    SerconPrintLiteral `BiosDetectMemory:\n`
    mov word [bp-44], .map
    mov word [bp-46], .capacity
    xor ebx, ebx
.loop:
    mov eax, 0xE820
    mov edx, .smap
    mov ecx, 24
    BeginWithSegment es, ss
        lea di, [bp-24]
        int 0x15
    EndWithSegment
    jc .carry
    cmp eax, .smap
    jne .magic
    test ebx, ebx
    jz .token
    lea di, [bp-42]
    mov si, di
    mov cx, 9
    SerconPrintLiteral `    token `
    mov eax, ebx
    CallWithEs ss, ToHex32
    CallWithDs ss, SerconPrint
    add cx, 8
    SerconPrintLiteral ` base `
    mov edx, [bp-20]
    mov eax, [bp-24]
    CallWithEs ss, ToHex64
    CallWithDs ss, SerconPrint
    SerconPrintLiteral ` length `
    mov edx, [bp-12]
    mov eax, [bp-16]
    CallWithEs ss, ToHex64
    CallWithDs ss, SerconPrint
    sub cx, 8
    SerconPrintLiteral ` type `
    mov eax, [bp-8]
    CallWithEs ss, ToHex32
    CallWithDs ss, SerconPrint
    SerconPrintLiteral `\n`
    cmp eax, 1
    jne .unusable                   ; skip if type != 1
    mov eax, [bp-16]
    or eax, [bp-12]
    jz .unusable                    ; skip if length = 0
    cmp word [bp-46], 0
    je .overflow
    BeginWithSegment ds, ss
        lea si, [bp-24]
        mov di, [bp-44]
        mov cx, 16
        rep movsb
        mov [bp-44], di
    EndWithSegment
    dec word [bp-46]
.unusable:
    jmp .loop
.overflow:
    SerconPrintLiteral `    exiting due to buffer capacity\n`
    jmp .out
.carry:
    SerconPrintLiteral `    exiting due to carry (CF = 1)\n`
    jmp .out
.magic:
    SerconPrintLiteral `    exiting due to magic (EAX != 0x534D4150)\n`
    jmp .out
.token:
    SerconPrintLiteral `    exiting due to token (EBX = 0)\n`
.out:
    o32 popa
    leave
    ret

Idle:
    SerconPrintLiteral `>>> Idle: waiting for interrupts\n`
.loop:
    enter 0, 0
    push 0x1234
    push 0x1234
    hlt
    ;SerconPrintLiteral `>>> Idle: HLT instruction ended\n`
    leave
    jmp .loop

PitTick:
    enter 0, 0
    pusha
    SerconPrintLiteral "."
    ;SerconPrintLiteral `>>> PitTick\n`
    ;DebugDumpRegs 4
    Outb Pic.base1, 0x20
    popa
    leave
    iret

Pic:
.base1: equ 0x20
.base2: equ 0xA0
.index1: equ 0xF0                   ; IRQ0 @ INT F0h
.index2: equ 0xF8                   ; IRQ8 @ INT F8h
    enter 6, 0
    pusha
    lea di, [bp-6]
    lea si, [di+2]
    mov cx, 3
    SerconPrintLiteral `Pic: remapping IRQ0 to INT `
    mov al, .index1
    CallWithEs ss, ToHex
    CallWithDs ss, SerconPrint
    SerconPrintLiteral `\nPic: remapping IRQ8 to INT `
    mov al, .index2
    CallWithEs ss, ToHex
    CallWithDs ss, SerconPrint
    SerconPrintLiteral `\n`
    In al, .base1+1
    xchg cx, ax
    In al, .base2+1
    xchg bx, ax
    Outb .base1, 0x11
    Outb .base2, 0x11
    Outb .base1+1, .index1
    Outb .base2+1, .index2
    Outb .base1+1, 0x04
    Outb .base2+1, 0x02
    Outb .base1+1, 0x01
    Outb .base2+1, 0x01
    Outb .base1+1, cl
    Outb .base2+1, bl
    popa
    leave
    ret

DumpRegs:                           ; in cx, [fs:si]
    enter 10, 0
    o32 pusha
    push cx
    mov al, ' '
    jcxz .out0
.indent0:
    call SerconWrite
    loop .indent0
.out0:
    mov bx, si
    lea di, [bp-10]
    mov si, di
    mov cx, 9
    SerconPrintLiteral `EIP=`
    mov eax, fs:[bx+60]
    CallWithEs ss, ToHex32
    CallWithDs ss, SerconPrint
    SerconPrintLiteral ` EFLAGS=`
    mov eax, fs:[bx+24]
    CallWithEs ss, ToHex32
    CallWithDs ss, SerconPrint
    SerconPrintLiteral `\n`
    pop cx
    lea si, fs:[bx+28]
    call DumpGprs
    lea si, fs:[bx+0]
    call DumpSregs
    mov al, ' '
    jcxz .out1
.indent1:
    call SerconWrite
    loop .indent1
.out1:
    SerconPrintLiteral `GDTR=`
    lea si, fs:[bx+12]
    call DumpXdtr
    SerconPrintLiteral ` IDTR=`
    lea si, fs:[bx+18]
    call DumpXdtr
    SerconPrintLiteral `\n`
    o32 popa
    leave
    ret

DumpXdtr:                           ; in [fs:si]
    enter 10, 0
    pusha
    mov bx, si
    lea di, [bp-10]
    mov si, di
    mov cx, 8
    mov eax, fs:[bx+2]
    CallWithEs ss, ToHex32
    CallWithDs ss, SerconPrint
    SerconPrintLiteral `+`
    sub cx, 3
    mov ax, fs:[bx+0]
    CallWithEs ss, ToHex
    CallWithDs ss, SerconPrint
    popa
    leave
    ret

DumpSregs:                          ; in cx, [fs:si]
    enter 6, 0
    pusha
    mov al, ' '
    jcxz .out
.indent:
    call SerconWrite
    loop .indent
.out:
    mov bx, si
    lea di, [bp-6]
    mov si, di
    mov cx, 5
    SerconPrintLiteral `CS=`
    mov ax, fs:[bx+0]
    CallWithEs ss, ToHex
    CallWithDs ss, SerconPrint
    SerconPrintLiteral ` SS=`
    mov ax, fs:[bx+2]
    CallWithEs ss, ToHex
    CallWithDs ss, SerconPrint
    SerconPrintLiteral ` DS=`
    mov ax, fs:[bx+4]
    CallWithEs ss, ToHex
    CallWithDs ss, SerconPrint
    SerconPrintLiteral ` ES=`
    mov ax, fs:[bx+6]
    CallWithEs ss, ToHex
    CallWithDs ss, SerconPrint
    SerconPrintLiteral ` FS=`
    mov ax, fs:[bx+8]
    CallWithEs ss, ToHex
    CallWithDs ss, SerconPrint
    SerconPrintLiteral ` GS=`
    mov ax, fs:[bx+10]
    CallWithEs ss, ToHex
    CallWithDs ss, SerconPrint
    mov al, `\n`
    call SerconWrite
    popa
    leave
    ret

DumpGprs:                           ; in cx, [fs:si]
    enter 10, 0
    o32 pusha
    push cx
    mov al, ' '
    jcxz .out0
.indent0:
    call SerconWrite
    loop .indent0
.out0:
    mov bx, si
    lea di, [bp-10]
    mov si, di
    mov cx, 9
    SerconPrintLiteral `EAX=`
    mov eax, fs:[bx+28]
    CallWithEs ss, ToHex32
    CallWithDs ss, SerconPrint
    SerconPrintLiteral ` ECX=`
    mov eax, fs:[bx+24]
    CallWithEs ss, ToHex32
    CallWithDs ss, SerconPrint
    SerconPrintLiteral ` EDX=`
    mov eax, fs:[bx+20]
    CallWithEs ss, ToHex32
    CallWithDs ss, SerconPrint
    SerconPrintLiteral ` EBX=`
    mov eax, fs:[bx+16]
    CallWithEs ss, ToHex32
    CallWithDs ss, SerconPrint
    SerconPrintLiteral `\n`
    pop cx
    mov al, ' '
    jcxz .out1
.indent1:
    call SerconWrite
    loop .indent1
.out1:
    mov cx, 9
    SerconPrintLiteral `ESP=`
    mov eax, fs:[bx+12]
    CallWithEs ss, ToHex32
    CallWithDs ss, SerconPrint
    SerconPrintLiteral ` EBP=`
    mov eax, fs:[bx+8]
    CallWithEs ss, ToHex32
    CallWithDs ss, SerconPrint
    SerconPrintLiteral ` ESI=`
    mov eax, fs:[bx+4]
    CallWithEs ss, ToHex32
    CallWithDs ss, SerconPrint
    SerconPrintLiteral ` EDI=`
    mov eax, fs:[bx+0]
    CallWithEs ss, ToHex32
    CallWithDs ss, SerconPrint
    mov al, `\n`
    call SerconWrite
    o32 popa
    leave
    ret

%include "stage01.s"
