%ifndef BOOP_MACROS_SECTIONS_S
%define BOOP_MACROS_SECTIONS_S

%macro DefString 3+
[section %1]
%2:
%2.value: %3
%2.len: equ $-%2.value
__?SECT?__
%endmacro

%macro Def 3+
[section %1]
%2: %3
__?SECT?__
%endmacro

%endif
