%ifndef BOOP_MACROS_PORTIO_S
%define BOOP_MACROS_PORTIO_S

%macro In 2
    mov dx, %2
    in %1, dx
%endmacro

%macro Out 3
    mov dx, %2
    mov %1, %3
    out dx, %1
%endmacro

%macro Outb 2
    Out al, %1, %2
%endmacro

%macro Outw 2
    Out ax, %1, %2
%endmacro

%endif
