%ifndef BOOP_MACROS_DEBUG_S
%define BOOP_MACROS_DEBUG_S

%include "macros/sections.s"

DebugPushRegs.len: equ 64
DebugPushRegs.eip: equ 60
DebugPushRegs.esp: equ 40
DebugPushRegs.ebp: equ 36
DebugPushRegs.idtr: equ 18
DebugPushRegs.gdtr: equ 12
%macro DebugPushRegs 0
%%len: equ DebugPushRegs.len
%%eip: equ DebugPushRegs.eip
%%esp: equ DebugPushRegs.esp
%%ebp: equ DebugPushRegs.ebp
%%idtr: equ DebugPushRegs.idtr
%%gdtr: equ DebugPushRegs.gdtr
%%start:
    push 0                          ; - 2 @ 62
    call %%ip                       ; - 2 @ 60
%%ip:
    o32 pusha                       ; - 32 @ 28
    o32 pushf                       ; - 4 @ 24
    sub sp, 12                      ; - 12 @ 12
    push gs                         ; - 2 @ 10
    push fs                         ; - 2 @ 8
    push es                         ; - 2 @ 6
    push ds                         ; - 2 @ 4
    push ss                         ; - 2 @ 2
    push cs                         ; - 2 @ 0
    enter 0, 0
    pusha
    lea di, [bp+2+%%idtr]           ; @ 18
    sidt [di]
    lea di, [bp+2+%%gdtr]           ; @ 12
    sgdt [di]
    sub word [bp+2+%%eip], %%ip-%%start
    add word [bp+2+%%esp], 4
    popa
    leave
%endmacro

%macro SerconPrintLiteral 1+
    DefString .data, %%message, db %1
    SerconPrintString %%message
%endmacro

%macro SerconPrintString 1
    pusha
    mov cx, %1.len
    mov si, %1.value
    call SerconPrint
    popa
%endmacro

%endif
