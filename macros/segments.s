%ifndef BOOP_MACROS_SEGMENTS_S
%define BOOP_MACROS_SEGMENTS_S

%macro BeginWithSegment 2
%push BeginWithSegment
%define %$segment %1
    push %$segment
    push %2
    pop %$segment
%endmacro

%macro EndWithSegment 0
    pop %$segment
%pop BeginWithSegment
%endmacro

%macro CallWithSegment 3
    BeginWithSegment %1, %2
        call %3
    EndWithSegment
%endmacro

%macro CallWithDs 2
    CallWithSegment ds, %1, %2
%endmacro

%macro CallWithEs 2
    CallWithSegment es, %1, %2
%endmacro

%macro CallWithFs 2
    CallWithSegment fs, %1, %2
%endmacro

%endif
