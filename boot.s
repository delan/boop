[map boot.map]
bits 16
org 0x7C00

section .data follows=.text
section .mbr start=0x7DFE
    dw 0xAA55
section .text

%include "macros/debug.s"
%include "macros/portio.s"
%include "macros/sections.s"

InitRegs:
    DebugPushRegs
    mov ax, ss
    mov ds, ax
    mov si, sp                      ; ds:si := ss:sp
    xor ax, ax
    mov es, ax
    mov di, 0x1000                  ; es:di := 0h:1000h
    mov cx, DebugPushRegs.len
    rep movsb
    mov ss, ax
    mov ds, ax
    xor sp, sp
    xor bp, bp
    jmp 0x0000:Sercon               ; for machines with CS=07C0h

Sercon:
.baud: equ 9600
    pusha
    Outb .base+1, 0x00
    Outb .base+3, 0x80
    Outw .base+0, 115200/.baud
    Outb .base+3, 0x03              ; 8N1
    Outb .base+2, 0xC7
.test:
    ;Outb .base+4, 0x0B
    ;Outb .base+4, 0x1E
    ;Outb .base, 0xAE
    ;xor ax, ax
    ;In al, .base
    ;jmp $
.start:
    Outb .base+4, 0x0F
    popa

Stage0:
    SerconPrintLiteral `\n\nboop\n>>> stage0\n`

ReadStage1:
    SerconPrintLiteral `read`
    mov cx, .sectors/.chunk
.loop:
    mov al, '?'
    call SerconWrite
    mov si, .dapack
    mov ah, 0x42
    int 0x13
    jc .error
    add word [.offset], .chunk*0x200
    add dword [.block], .chunk
    loop .loop
    SerconPrintLiteral ` ok\n`
    xor sp, sp
    xor bp, bp
    mov ax, 0x2000
    mov ss, ax
    mov ax, 0x1000
    mov ds, ax
    mov es, ax
    xor ax, ax
    mov fs, ax
    mov gs, ax
    jmp 0x1000:0x0000
.error:
    mov al, ' '
    call SerconWrite
    enter 6, 0
    lea di, [bp-6]
    lea si, [di+2]
    mov cx, 3
    xchg al, ah
    call ToHex
    call SerconPrint
    jmp Hang
.sectors: equ 128
.chunk: equ 64
[section .data]
.dapack:
    db 0x10, 0
    dw .chunk
.offset:
    dw 0x0000                       ; buffer offset
    dw 0x1000                       ; buffer seg
.block:
    dd 0x00000001                   ; LBA48 low
    dd 0x0000                       ; LBA48 high
__?SECT?__

Hang:
    cli
    hlt
    jmp $

%include "stage01.s"
